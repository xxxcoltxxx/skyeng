<?php

namespace App\Decorator;

use App\Integration\DataProvider;
use DateTime;
use Psr\Cache\CacheItemPoolInterface;

class DecoratorManager
{
    /**
     * @var CacheItemPoolInterface
     */
    public $cache;

    /**
     * @var DataProvider
     */
    protected $dataProvider;

    public function __construct(DataProvider $dataProvider, CacheItemPoolInterface $cache)
    {
        $this->dataProvider = $dataProvider;
        $this->cache = $cache;
    }

    public function get(array $input)
    {
        $cacheKey = $this->getCacheKey($input);
        $cacheItem = $this->cache->getItem($cacheKey);
        if ($cacheItem->isHit()) {
            return $cacheItem->get();
        }

        $result = $this->dataProvider->get($input);

        $cacheItem
            ->set($result)
            ->expiresAt(
                (new DateTime())->modify('+1 day')
            );

        return $result;
    }

    public function getCacheKey(array $input)
    {
        // Другого варианта в данном примере не получится придумать, т.к. нужен уникальный ключ dataProvider
        return get_called_class() . ":" . $this->dataProvider->getHost() . ":" . json_encode($input);
    }
}

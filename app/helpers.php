<?php

/**
 * Вычисляет сумму двух чисел, даже если они выходят за пределы [PHP_INT_MIN, PHP_INT_MAX].
 *
 * @param string $p1
 * @param string $p2
 * @return string
 */
function sum(string $p1, string $p2): string
{
    $isNegative1 = $p1[0] === '-';
    $isNegative2 = $p2[0] === '-';
    $p1 = ltrim($p1, '-');
    $p2 = ltrim($p2, '-');
    $len = max(strlen($p1), strlen($p2));
    $p1 = str_pad($p1, $len, '0', STR_PAD_LEFT);
    $p2 = str_pad($p2, $len, '0', STR_PAD_LEFT);
    $prefix = '';
    if ($isNegative1 && $isNegative2 || $isNegative1 && $p1 >= $p2 || $isNegative2 && $p1 < $p2) {
        $prefix = '-';
    }

    $k1 = $isNegative1 ? -1 : 1;
    $k2 = $isNegative2 ? -1 : 1;
    $k = $k1 * $k2;
    $digits = strlen(PHP_INT_MAX) - 2;

    $result = '';
    $next = 0;
    for ($pos = $len; $pos > 0; $pos -= $digits) {
        $start = max(0, $pos - $digits);
        $length = min($pos, $digits);

        $d1 = intval(substr($p1, $start, $length)) * $k1;
        $d2 = intval(substr($p2, $start, $length)) * $k2;

        $sum = $d1 + $d2 + $next;

        $overflow = pow(10, $digits);

        if ($k > 0) {
            $next = $sum >= $overflow ? 1 : ($sum <= -$overflow ? -1 : 0);
        } else {
            $next = $sum < 0 ? -1 : 0;
        }

        $result = str_pad(abs($sum % ($overflow)), $length, '0', STR_PAD_LEFT) . $result;
    }

    return $prefix . ltrim(($next > 0 ? $next : '') . $result, '0') ?: 0;
}

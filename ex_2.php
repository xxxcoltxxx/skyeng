<?php

$filename = 'counter.txt';
if (! file_exists($filename)) {
    touch($filename);
}

$fp = fopen($filename, 'r+');
if (flock($fp, LOCK_EX)) {
    $count = '';
    while ($str = fread($fp, 1024)) {
        $count .= $str;
    }
    $next = intval(trim($count)) + 1;
    ftruncate($fp, 0);
    fwrite($fp, $next);
    fflush($fp);
    flock($fp, LOCK_UN);
} else {
    var_dump('Cannot lock file');
}

fclose($fp);


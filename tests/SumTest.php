<?php

namespace Tests;

class SumTest extends TestCase
{
    public function provider_sum()
    {
        return [
            ['112345' . str_repeat('3', 100), '12345' . str_repeat('3', 100), '124690' . str_repeat('6', 100)],
            ['124690' . str_repeat('3', 100), '-12345' . str_repeat('3', 100), '112345' . str_repeat('0', 100)],
            ['1' . str_repeat('0', 100), '-2' . str_repeat('0', 100), '-1' . str_repeat('0', 100)],
            ['-12345' . str_repeat('3', 100), '212345' . str_repeat('3', 100), '200000' . str_repeat('0', 100)],
            ['12345' . str_repeat('3', 100), '-12345' . str_repeat('3', 100), '0'],
            ['12345' . str_repeat('3', 100), '12345' . str_repeat('3', 100), '24690' . str_repeat('6', 100)],
            ['-12345' . str_repeat('3', 100), '-12345' . str_repeat('3', 100), '-24690' . str_repeat('6', 100)],
            [str_repeat('9', 500), str_repeat('9', 500), '1' . str_repeat('9', 499) . '8'],
        ];
    }

    /**
     * @param string $p1 First value
     * @param string $p2 Second value
     * @param string $sum Expected sum
     *
     * @dataProvider provider_sum
     */
    public function test_sum($p1, $p2, $sum)
    {
        $result = sum($p1, $p2);

        $this->assertEquals($sum, $result);
    }
}
